
# coding: utf-8

# In[1]:


import numpy as np
import cPickle
import os
import gzip


# # First exercise: Classifying MNIST with MLPs
# In this exercise you will implement a Neural Network (or MLP) and classify the MNIST digits with it.
# MNIST is a "well hung" dataset that has been used a lot over the years to benchmark different classification algorithms. 
# To learn more about it have a look here: http://yann.lecun.com/exdb/mnist/ .

# # Data Loading
# We first define a function for downloading and loading MNIST.
# **WARNING**: Executing it will obviously use up some space on your machine ;). 

# In[2]:


def mnist(datasets_dir='./data'):
    if not os.path.exists(datasets_dir):
        os.mkdir(datasets_dir)
    data_file = os.path.join(datasets_dir, 'mnist.pkl.gz')
    if not os.path.exists(data_file):
        print('... downloading MNIST from the web')
        try:
            import urllib
            urllib.urlretrieve('http://google.com')
        except AttributeError:
            import urllib.request as urllib
        url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        urllib.urlretrieve(url, data_file)

    print('... loading data')
    # Load the dataset
    f = gzip.open(data_file, 'rb')
    try:
        train_set, valid_set, test_set = cPickle.load(f, encoding="latin1")
    except TypeError:
        train_set, valid_set, test_set = cPickle.load(f)
    f.close()

    test_x, test_y = test_set
    test_x = test_x.astype('float32')
    test_x = test_x.astype('float32').reshape(test_x.shape[0], 1, 28, 28)
    test_y = test_y.astype('int32')
    valid_x, valid_y = valid_set
    valid_x = valid_x.astype('float32')
    valid_x = valid_x.astype('float32').reshape(valid_x.shape[0], 1, 28, 28)
    valid_y = valid_y.astype('int32')
    train_x, train_y = train_set
    train_x = train_x.astype('float32').reshape(train_x.shape[0], 1, 28, 28)
    train_y = train_y.astype('int32')
    rval = [(train_x, train_y), (valid_x, valid_y), (test_x, test_y)]
    print('... done loading data')
    return rval


# In[3]:


mnist()


# # Neural Network Layers
# We now define "bare bone" neural network layers.
# The parts marked with **TODO** are where you should finish the implementation!
# Conceptually we will implement the layers as follows:
# 
# Each layer has a constructor that takes an input layer plus some additional arguments such as layer size and the activation function name. The layer then uses the provided input layer to compute the layer dimensions, weight shapes, etc. and setup all auxilliary variables.
# 
# Each layer then has to provide three functions (as defined in the Layer class below): *output_shape()*, *fprop()* and *brop()*. The output_shape function is used to figure out the shape for the next layer and the *fprop()/bprop()* functions are used to compute forward and backward passes through the network.
# 
# Question: how to initialize? Web search tells us Xavier initialization is reasonable, see http://andyljones.tumblr.com/post/110998971763/an-explanation-of-xavier-initialization. $n_i$ is the number of input units per unit, thus we choose a normal distribution with $\mu = 0$ and $\sigma = \sqrt{\frac{1}{\text{num_prev_units}}}$.
# 
# I just see there is an initialization variable `init_stddev` we can use.

# In[4]:


# start by defining simple helpers
def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def sigmoid_d(x):
    return sigmoid(x) * (1 - sigmoid(x))

def tanh(x):
    return np.tanh(x)

def tanh_d(x):
    return 1 - np.tanh(x) ** 2

def relu(x):
    return np.maximum(0.0, x)

def relu_d(x):
    return np.vectorize(lambda x: 1 if x>0 else 0)(x) # compute the left differential

def softmax(x, axis=1):
    # to make the softmax a "safe" operation we will 
    # first subtract the maximum along the specified axis
    # so that np.exp(x) does not blow up!
    # Note that this does not change the output.
    x_max = np.max(x, axis=axis, keepdims=True)
    x_safe = x - x_max
    e_x = np.exp(x_safe)
    return e_x / np.sum(e_x, axis=axis, keepdims=True)

def one_hot(labels):
    """this creates a one hot encoding from a flat vector:
    i.e. given y = [0,2,1]
     it creates y_one_hot = [[1,0,0], [0,0,1], [0,1,0]]
    """
    classes = np.unique(labels)
    n_classes = classes.size
    one_hot_labels = np.zeros(labels.shape + (n_classes,))
    for c in classes:
        one_hot_labels[labels == c, c] = 1
    return one_hot_labels

def unhot(one_hot_labels):
    """ Invert a one hot encoding, creating a flat vector """
    return np.argmax(one_hot_labels, axis=-1)

# then define an activation function class
class Activation(object):
    
    def __init__(self, tname):
        if tname == 'sigmoid':
            self.act = sigmoid
            self.act_d = sigmoid_d
        elif tname == 'tanh':
            self.act = tanh
            self.act_d = tanh_d
        elif tname == 'relu':
            self.act = relu
            self.act_d = relu_d
        elif tname == 'linear':
            self.act = lambda x: x
            self.act_d = lambda x: np.ones(shape=x.shape)
        else:
            raise ValueError('Invalid activation function.')
            
    def fprop(self, input):
        # we need to remember the last input
        # so that we can calculate the derivative with respect
        # to it later on
        self.last_input = input
        return self.act(input)
    
    def bprop(self, output_grad):
        # print output_grad.shape, self.last_input.shape
        return output_grad * self.act_d(self.last_input)

# define a base class for layers
class Layer(object):
    
    def fprop(self, input):
        """ Calculate layer output for given input 
            (forward propagation). 
        """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def bprop(self, output_grad):
        """ Calculate input gradient and gradient 
            with respect to weights and bias (backpropagation). 
        """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def output_size(self):
        """ Calculate size of this layer's output.
        input_shape[0] is the number of samples in the input.
        input_shape[1:] is the shape of the feature.
        """
        raise NotImplementedError('This is an interface class, please use a derived instance')

# define a base class for loss outputs
# an output layer can then simply be derived
# from both Layer and Loss 
class Loss(object):

    def loss(self, output, output_net):
        """ Calculate mean loss given real output and network output. """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def input_grad(self, output, output_net):
        """ Calculate input gradient real output and network output. """
        raise NotImplementedError('This is an interface class, please use a derived instance')

# define a base class for parameterized things        
class Parameterized(object):
    
    def params(self):
        """ Return parameters (by reference) """
        raise NotImplementedError('This is an interface class, please use a derived instance')
    
    def grad_params(self):
        """ Return accumulated gradient with respect to params. """
        raise NotImplementedError('This is an interface class, please use a derived instance')

# define a container for providing input to the network
class InputLayer(Layer):
    
    def __init__(self, input_shape):
        if not isinstance(input_shape, tuple):
            raise ValueError("InputLayer requires input_shape as a tuple")
        self.input_shape = input_shape

    def output_size(self):
        return self.input_shape
    
    def fprop(self, input):
        self.input_shape = input.shape
        return input
    
    def bprop(self, output_grad):
        return output_grad
        
class FullyConnectedLayer(Layer, Parameterized):
    """ A standard fully connected hidden layer, as discussed in the lecture.
    """
    
    def __init__(self, input_layer, num_units, 
                 init_stddev, activation_fun=Activation('relu')):
        self.num_units = num_units
        self.activation_fun = activation_fun
        # the input shape will be of size (batch_size, num_units_prev) 
        # where num_units_prev is the number of units in the input 
        # (previous) layer
        self.input_layer = input_layer
        self.input_shape = input_layer.output_size()
        (batch_size, num_units_prev) = self.input_shape
        # TODO ################################
        # TODO: implement weight initialization
        # TODO ################################
        # W is the weight matrix, it should have shape: (num_units_prev, num_units)
        # (fg: I prefer the transposed of W)
        # and b is the bias vector of shape: (num_units)
        
        # QUES: use Xavier initialization?
        # init_stddev = np.sqrt(1 / num_units_prev)
        self.W = np.random.normal(0, init_stddev, (num_units, num_units_prev))
        self.b = np.random.normal(0, init_stddev, (num_units,))
        
        # create dummy variables for parameter gradients
        # no need to change these here!
        self.dW = None
        self.db = None
    
    def output_size(self):
        return (self.input_shape[0], self.num_units)
    
    def fprop(self, inp):
        # TODO ################################################
        # TODO: implement forward propagation
        # NOTE: you should also handle the case were 
        #       activation_fun is None (meaning no activation)
        #       then this is simply a linear layer
        # TODO ################################################
        # you again want to cache the last_input for the bprop
        # implementation below!
        self.input_shape = self.input_layer.output_size()
        self.last_input = inp
        if self.activation_fun is None:
            self.activation_fun = Activation('linear')
        # print self.W.shape, inp.T.shape, self.input_shape, self.output_size()
        batch_size = self.input_shape[0]
        # print "fprop: ", self.input_shape, batch_size
        outp = self.activation_fun.fprop(np.dot(self.W, inp.T).T + np.tile(self.b, (batch_size,1)))
        # print outp.shape, self.output_size()
        assert outp.shape == self.output_size()
        return outp
        
    def bprop(self, output_grad):
        """ Calculate input gradient (backpropagation). """
        # TODO ################################
        # TODO: implement backward propagation
        # TODO ###############################
        
        # HINT: you may have to divide the weights by batch_size
        #       to make gradient checking work 
        #       (since you want to divide the loss by number of inputs)
        batch_size = output_grad.shape[0]
        # accumulate gradient wrt. the parameters first
        # we will need to store these to later update
        # the network after a few forward backward passes
        # the gradient wrt. W should be stored as self.dW
        # the gradient wrt. b should be stored as self.db
        # print "OUT: ", output_grad.shape, "W: ", self.W.shape
        
        grad_act = self.activation_fun.bprop(output_grad)
        # print grad_act.shape, self.last_input.shape
        self.dW = np.matmul(grad_act[:, :, None], self.last_input[:, None, :])
        self.db = grad_act
        # print "backprop: dW, db = ", self.dW, self.db
               
        # the gradient wrt. the input should be calculated here
        grad_input = np.matmul(grad_act, self.W)
        return grad_input
        
    def params(self):
        return self.W, self.b

    def grad_params(self):
        return self.dW, self.db

# finally we specify the interface for output layers 
# which are layers that also have a loss function
# we will implement two output layers:
#  a Linear, and Softmax (Logistic Regression) layer
# The difference between output layers and and normal 
# layers is that they will be called to compute the gradient
# of the loss through input_grad(). bprop will never 
# be called on them!
class LinearOutput(Layer, Loss):
    """ A simple linear output layer that  
        uses a squared loss (e.g. should be used for regression)
    """
    def __init__(self, input_layer):
        self.input_size = input_layer.output_size()
        
    def output_size(self):
        return (1,)
        
    def fprop(self, input):
        return input

    def bprop(self, output_grad):
        raise NotImplementedError(
            'LinearOutput should only be used as the last layer of a Network'
            + ' bprop() should thus never be called on it!'
        )
    
    def input_grad(self, Y, Y_pred):
        # TODO ####################################
        # TODO: implement gradient of squared loss
        # TODO ####################################
        return Y_pred - Y

    def loss(self, Y, Y_pred):
        loss = 0.5 * np.square(Y - Y_pred)
        return np.mean(np.sum(loss, axis=1))

class SoftmaxOutput(Layer, Loss):
    """ A softmax output layer that calculates 
        the negative log likelihood as loss
        and should be used for classification.
    """
    
    def __init__(self, input_layer):
        self.input_size = input_layer.output_size()
        
    def output_size(self):
        return (1,)
    
    def fprop(self, input):
        return softmax(input)
    
    def bprop(self, output_grad):
        raise NotImplementedError(
            'SoftmaxOutput should only be used as the last layer of a Network'
            + ' bprop() should thus never be called on it!'
        )
    
    def input_grad(self, Y, Y_pred):
        # TODO #######################################################
        # TODO: implement gradient of the negative log likelihood loss
        # TODO #######################################################
        # HINT: since this would involve taking the log 
        #       of the softmax (which is np.exp(x)/np.sum(np.exp(x), axis=1))
        #       this gradient computation can be simplified a lot!
        # return Y * softmax(Y_pred - 1)  # wrong
        # batch_size = Y.shape[0]
        # print batch_size
        return (Y_pred - Y) # / batch_size
        
    def loss(self, Y, Y_pred):
        # Assume one-hot encoding of Y
        # calculate softmax first
        # out = softmax(Y_pred)
        out = Y_pred
        # to make the loss numerically stable 
        # you may want to add an epsilon in the log ;)
        eps = 1e-10
        # TODO ####################################
        # calculate negative log likelihood
        # TODO ####################################
        np.clip(out, eps, 1, out=out)
        # print Y, out
        loss = - np.sum(Y * np.log(out), axis=1)
        return np.mean(loss)
        


# In[5]:


# check derivative for softmax:

S = SoftmaxOutput(InputLayer((1,3)))
from scipy.optimize import approx_fprime, check_grad
Y = np.array([[1,0,0]])
Y_pred = np.array([[0.3, 0.4, 0.2]])
print "Calculated gradient: ", S.input_grad(Y, Y_pred)
f = lambda Y_pred: S.loss(Y, Y_pred)
grad_fun = lambda Y_pred: S.input_grad(Y, Y_pred)
eps = np.sqrt(np.finfo(float).eps)
n = len(Y_pred.ravel())
loss_base = f(Y_pred)

print "Finite differences:",
for i in range(3):
    print (f(softmax(Y_pred + eps * np.eye(1, n, i))) - f(softmax(Y_pred))) / eps,
print "\n"
    
dx_i = lambda i: (f(Y_pred + eps * np.eye(1, n, i))
                  - loss_base) / eps
dx = np.vectorize(dx_i)
gparam_fd = dx(np.arange(n))
print "Vectorized FD: ", gparam_fd



# # Neural Network class
# With all layers in place (and properly implemented by you) we can finally define a neural network.
# For our purposes a neural network is simply a collection of layers which we will cycle through and on which we will call fprop and bprop to compute partial derivatives with respect to the input and the parameters.
# 
# Pay special attention to the `check_gradients()` function in which you should implement automatic differentiation. This function will become your best friend when checking the correctness of your implementation.
# 
# **TODO**: currently, we assume layers `1:-1` are Parametrized, we should better use `isinstance` and check. Same for `fprop` / `bprop`: use previous layer attribute, not index in layers stack!

# In[18]:


class NeuralNetwork:
    """ Our Neural Network container class.
    """
    def __init__(self, layers):
        self.layers = layers
        
    def _loss(self, X, Y):
        Y_pred = self.predict(X)
        return self.layers[-1].loss(Y, Y_pred)

    def predict(self, X):
        """ Calculate an output Y for the given input X. """
        # TODO ##########################################
        # TODO: implement forward pass through all layers
        # TODO ##########################################
        Y_pred = X
        for layer in self.layers:
            Y_pred = layer.fprop(Y_pred)
        return Y_pred
    
    def backpropagate(self, Y, Y_pred, upto=0):
        """ Backpropagation of partial derivatives through 
            the complete network up to layer 'upto'
        """
        next_grad = self.layers[-1].input_grad(Y, Y_pred)
        # TODO ##########################################
        # TODO: implement backward pass through all layers
        # TODO ##########################################
        for l in range(len(self.layers) - 2, upto - 1, -1):
            # print "backprop: input_grad = ", next_grad
            layer = self.layers[l]
            next_grad = layer.bprop(next_grad)
        # print "backprop: input_grad = ", next_grad
        return next_grad
    
    def classification_error(self, X, Y):
        """ Calculate error on the given data 
            assuming they are classes that should be predicted. 
        """
        Y_pred = unhot(self.predict(X))
        error = Y_pred != Y
        return np.mean(error)
    
    def sgd_epoch(self, X, Y, learning_rate, batch_size):
        n_samples = X.shape[0]
        n_batches = n_samples // batch_size
        for b in range(n_batches):
            # TODO #####################################
            # Implement stochastic gradient descent here
            # TODO #####################################
            # start by extracting a batch from X and Y
            # (you can assume the inputs are already shuffled)
            X_batch = X[b * batch_size : (b+1) * batch_size]
            Y_batch = Y[b * batch_size : (b+1) * batch_size]
            
            # TODO: then forward and backward propagation + updates
            # HINT: layer.params() returns parameters *by reference*
            #       so you can easily update in-place
            Y_pred = self.predict(X_batch)
            self.backpropagate(Y_batch, Y_pred)
            for layer in self.layers[1:-1]:
                layer.W -= learning_rate * np.mean(layer.dW, axis=0)
                layer.b -= learning_rate * np.mean(layer.db, axis=0)
    
    def gd_epoch(self, X, Y, learning_rate, batch_size=0):
        # TODO ##################################################
        # Implement batch gradient descent here
        # A few hints:
        #   There are two strategies you can follow:
        #   Either shove the whole dataset through the network
        #   at once (which can be problematic for large datasets)
        #   or run through it batch-wise as in the sgd approach
        #   and accumulate the gradients for all parameters as
        #   you go through the data. Either way you should then
        #   do one gradient step after you went through the
        #   complete dataset!
        # TODO ##################################################
        n_samples = X.shape[0]
        if batch_size == 0:
            batch_size = n_samples
        n_batches = n_samples // batch_size
        
        for layer in self.layers[1:-1]:  # initialize batch gradient list
            layer.dW_accum = np.zeros_like(layer.W, dtype=float)
            layer.db_accum = np.zeros_like(layer.b, dtype=float)
        
        for b in range(n_batches):
            # print "Calculating gradient for batch {} of {}".format(b, n_batches)
            # extract batch
            X_batch = X[b * batch_size : (b+1) * batch_size]
            Y_batch = Y[b * batch_size : (b+1) * batch_size]
            
            # then forward and backward propagation + updates
            Y_pred = self.predict(X_batch)
            self.backpropagate(Y_batch, Y_pred)
            for layer in self.layers[1:-1]:
                layer.dW_accum += np.mean(layer.dW, axis=0)  # accumulate batch gradient 
                layer.db_accum += np.mean(layer.db, axis=0)
                
        for layer in self.layers[1:-1]:
            layer.dW = layer.dW_accum / n_batches  # total gradient
            layer.db = layer.db_accum / n_batches
            print "gd: gradient norm = ", np.linalg.norm(layer.dW)
            layer.W -= learning_rate * layer.dW  # gradient step
            layer.b -= learning_rate * layer.db
            
    def train(self, X, Y, learning_rate=0.1, adaptive=False, max_epochs=100, 
              batch_size=64, descent_type="sgd", y_one_hot=True,
              X_validation=None, Y_validation=None):
        import matplotlib.pyplot as plt
        """ Train network on the given data. """
        n_samples = X.shape[0]
        n_batches = n_samples // batch_size
        if y_one_hot:
            Y_train = one_hot(Y)
            Y_val = one_hot(Y_validation)
        else:
            Y_train = Y
            Y_val = one_hot(Y_validation)
        
        print("... starting training")
        for e in range(max_epochs+1):
            if adaptive:
                lr = learning_rate / (e + 1)
            else:
                lr = learning_rate
            if descent_type == "sgd":
                self.sgd_epoch(X, Y_train, lr, batch_size)
            elif descent_type == "gd":
                self.gd_epoch(X, Y_train, lr, batch_size)
            else:
                raise NotImplementedError("Unknown gradient descent type {}".format(descent_type))

            # Output error on the training data
            train_loss = self._loss(X, Y_train)
            train_error = self.classification_error(X, Y)
            print('epoch {:>2}:  loss {:.4f}, train error {:.4f}'.format(e, train_loss, train_error))
            # TODO ##################################################
            # compute error on validation data:
            # simply make the function take validation data as input
            # and then compute errors here and print them
            # TODO ##################################################
            # plt.matshow(self.layers[1].W)
            if X_validation is not None and e % 5 == 0:
                # Output error on the validation data for interediate checking
                validation_loss = self._loss(X_validation, Y_val)
                validation_error = self.classification_error(X_validation, Y_validation)
                print('Validation loss {:.4f}, eval  error {:.4f}'.format(validation_loss, validation_error))
                
        if X_validation is not None:
            # Output error on the validation data
            validation_loss = self._loss(X_validation, Y_val)
            validation_error = self.classification_error(X_validation, Y_validation)
            print('Validation loss {:.4f}, error {:.4f}'.format(validation_loss, validation_error))
                
    
    def check_gradients(self, X, Y):
        """ Helper function to test the parameter gradients for
        correctness. """
        batch_size = Y.shape[0]
        print Y.shape
        for l, layer in enumerate(self.layers[::-1]):
            if isinstance(layer, Parameterized):
                print('checking gradient for layer {}'.format(l))
                for p, param in enumerate(layer.params()):
                    # we iterate through all parameters
                    param_shape = param.shape
                    # define functions for conveniently swapping
                    # out parameters of this specific layer and 
                    # computing loss and gradient with these 
                    # changed parametrs
                    def output_given_params(param_new):
                        """ A function that will compute the output 
                            of the network given a set of parameters
                        """
                        # copy provided parameters
                        param[:] = np.reshape(param_new, param_shape)
                        # return computed loss
                        ret = self._loss(X, Y)
                        # print "Loss calc: ", ret
                        return ret

                    def grad_given_params(param_new):
                        """A function that will compute the gradient 
                           of the network given a set of parameters
                        """
                        # copy provided parameters
                        param[:] = np.reshape(param_new, param_shape)
                        # Forward propagation through the net
                        Y_pred = self.predict(X)
                        # Backpropagation of partial derivatives
                        self.backpropagate(Y, Y_pred, upto=l)
                        # return the computed gradient 
                        return np.ravel(layer.grad_params()[p])
                        
                    # let the initial parameters be the ones that
                    # are currently placed in the network and flatten them
                    # to a vector for convenient comparisons, printing etc.
                    param_init = np.ravel(np.copy(param))
                    
                    # TODO ####################################
                    # TODO compute the gradient with respect to
                    #      the initial parameters in two ways:
                    #      1) with grad_given_params()
                    #      2) with finite differences 
                    #         using output_given_params()
                    #         (as discussed in the lecture)
                    #      if your implementation is correct 
                    #      both results should be epsilon close
                    #      to each other!
                    # TODO ####################################
                    epsilon = 1e-4
                    # making sure your gradient checking routine itself 
                    # has no errors can be a bit tricky. To debug it
                    # you can "cheat" by using scipy which implements
                    # gradient checking exactly the way you should!
                    # To do that simply run the following here:
                    # import scipy.optimize
                    # err = scipy.optimize.check_grad(output_given_params, 
                    #                                 grad_given_params, param_init)
                    eps = np.sqrt(np.finfo(float).eps)
                    # grad_fd = scipy.optimize.approx_fprime(param_init, output_given_params, eps)
                    loss_base = output_given_params(param_init)
                    
                    # TODO this should hold the gradient as calculated through bprop
                    gparam_bprop = grad_given_params(param_init).reshape(batch_size, -1)
                    # TODO this should hold the gradient calculated through 
                    #      finite differences
                    n = len(param_init)
                    dx_i = lambda i: (output_given_params(param_init + eps * np.eye(1, n, i)) 
                                      - loss_base) / eps
                    dx = np.vectorize(dx_i)
                    gparam_fd = dx(np.arange(n))
                    gparam_fd = np.ravel(gparam_fd)
                    gparam_bprop = np.mean(gparam_bprop, axis=0)
                    # print "Check grads: ", gparam_bprop, gparam_fd
                    # calculate difference between them
                    # print gparam_bprop.shape, gparam_fd.shape
                    err = np.sum(np.abs(gparam_bprop - gparam_fd))
                    print('diff {:.2e}'.format(err))
                    assert(err < epsilon)
                    
                    # reset the parameters to their initial values
                    param[:] = np.reshape(param_init, param_shape)


# # Gradient Checking
# After implementing everything it is always a good idea to setup some layers and perform gradient
# checking on random data. **Note** that this is only an example! It is not a useful network architecture ;). We also expect you to play around with this to test all your implemented components.

# In[7]:


# check a very simple case
input_shape = (2, 2)
layers = [InputLayer(input_shape)]
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=4,
                init_stddev=0.2,
                activation_fun=None
))
layers.append(SoftmaxOutput(layers[-1]))
layers[1].W = np.arange(8, dtype=float).reshape(4,2)
layers[1].b = np.zeros(4, dtype=float)
nn = NeuralNetwork(layers)

X = np.array([1.,2.,1.,3.]).reshape(input_shape)
Y = np.array([1.,0.,0.,0.,0.,1.,0.,0.]).reshape(2,4)

# print nn.predict(X)

nn.check_gradients(X, Y)


# In[8]:


input_shape = (5, 10)
n_labels = 8
layers = [InputLayer(input_shape)]

layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=15,
                init_stddev=0.1,
                activation_fun=Activation('linear')
))
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=6,
                init_stddev=0.1,
                activation_fun=Activation('linear')
))
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=n_labels,
                init_stddev=0.1,
                activation_fun=Activation('linear')
))
layers.append(SoftmaxOutput(layers[-1]))
nn = NeuralNetwork(layers)


# In[9]:


# create random data
X = np.random.normal(size=input_shape)
# and random labels
Y = np.zeros(shape=(input_shape[0], n_labels))
for i in range(Y.shape[0]):
    idx = np.random.randint(n_labels)
    Y[i, idx] = 1.


# In[10]:


nn.check_gradients(X, Y)


# # Training on MNIST
# Finally we can let our network run on the MNIST dataset!

# First load the data and reshape it.

# In[11]:


# load
Dtrain, Dval, Dtest = mnist()
X_train, y_train = Dtrain
# Downsample training data to make it a bit faster for testing this code
n_train_samples = 60000
train_idxs = np.random.permutation(X_train.shape[0])[:n_train_samples]
X_train = X_train[train_idxs]
y_train = y_train[train_idxs]
X_val, Y_val = Dval


# *Dtrain* contains 50k images which are of size 28 x 28 pixels. Hence:

# In[12]:


print("X_train shape: {}".format(np.shape(X_train)))
print("y_train shape: {}".format(np.shape(y_train)))


# y_train will automatically be converted in the *train()* function to one_hot encoding.
# 
# 
# But we need to reshape X_train, as our Network expects flat vectors of size 28*28 as input!

# In[14]:


X_train = X_train.reshape(X_train.shape[0], -1)
X_val = X_val.reshape(X_val.shape[0], -1) 
print("Reshaped X_train size: {}".format(X_train.shape))


# Ah, much better ;-)! 
# 
# Now we can finally really start training a Network!
# 
# 
# I pre-defined a small Network for you below. Again This is not really a good default and will not produce state of the art results. Please play around with this a bit. See how different activation functions and training procedures (gd / sgd) affect the result.

# In[ ]:


import time

# Setup a small MLP / Neural Network
# we can set the first shape to None here to indicate that
# we will input a variable number inputs to the network
input_shape = (None, 28*28)
layers = [InputLayer(input_shape)]
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=400,
                init_stddev=0.125,
                activation_fun=Activation('relu')
))
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=400,
                init_stddev=0.1,
                activation_fun=Activation('relu')
))
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=10,
                init_stddev=0.1,
                # last layer has no nonlinearity 
                # (softmax will be applied in the output layer)
                activation_fun=None 
))
layers.append(SoftmaxOutput(layers[-1]))

nn = NeuralNetwork(layers)
# Train neural network
t0 = time.time()
nn.train(X_train, y_train, learning_rate=0.4, adaptive=False,
         max_epochs=40, batch_size=64, y_one_hot=True, descent_type='sgd',
         X_validation=X_val, Y_validation=Y_val)
t1 = time.time()
print('Duration: {:.1f}s'.format(t1-t0))




# # Figure out a reasonable Network that achieves good performance
# As the last part of this task, setup a network that works well and gets reasonable accuracy, say ~ 1-3 percent error on the **validation set**. 
# Train this network on the complete data and compute the **test error**. 
# 
# Once you have done this, visualize a few digits from the the test set that the network gets right as well as a few that the network gets wrong!
