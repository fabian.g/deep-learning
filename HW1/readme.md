## How to run the ex.1 notebook

(taken from https://github.com/aisrobots/dl_lab_2017/blob/master/notebooks/README.md)

1. Create virtual environment
      ```Shell
      virtualenv --no-site-packages -p python2 venv
      ```
2. Activate virtual environment
      ```Shell
      source venv/bin/activate 
      ```
3. Install libs
      ```Shell
      pip install ipython[notebook]
      pip install numpy
      pip install scipy
      ```
4. Run notebook
      ```Shell
      ipython notebook exercise_1.ipynb
      ```

