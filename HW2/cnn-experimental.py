# Homework 2 Deep Learning Lab Course
# Fabian Gloeckle

import tensorflow as tf
import os, gzip
import pickle as cPickle


def mnist(datasets_dir='./data'):
    """Download MNIST data set. (course repo)."""
    if not os.path.exists(datasets_dir):
        os.mkdir(datasets_dir)
    data_file = os.path.join(datasets_dir, 'mnist.pkl.gz')
    if not os.path.exists(data_file):
        print('... downloading MNIST from the web')
        try:
            import urllib
            urllib.urlretrieve('http://google.com')
        except AttributeError:
            import urllib.request as urllib
        url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        urllib.urlretrieve(url, data_file)

    print('... loading data')
    # Load the dataset
    f = gzip.open(data_file, 'rb')
    try:
        train_set, valid_set, test_set = cPickle.load(f, encoding="latin1")
    except TypeError:
        train_set, valid_set, test_set = cPickle.load(f)
    f.close()

    test_x, test_y = test_set
    test_x = test_x.astype('float32')
    test_x = test_x.astype('float32').reshape(test_x.shape[0], 1, 28, 28)
    test_y = test_y.astype('int32')
    valid_x, valid_y = valid_set
    valid_x = valid_x.astype('float32')
    valid_x = valid_x.astype('float32').reshape(valid_x.shape[0], 1, 28, 28)
    valid_y = valid_y.astype('int32')
    train_x, train_y = train_set
    train_x = train_x.astype('float32').reshape(train_x.shape[0], 1, 28, 28)
    train_y = train_y.astype('int32')
    rval = [(train_x, train_y), (valid_x, valid_y), (test_x, test_y)]
    print('... done loading data')
    return rval


# set up the CNN's architecture:
# https://www.tensorflow.org/tutorials/layers#building_the_cnn_mnist_classifier

def network(num_filters):

    x = tf.placeholder(tf.float32)
    y = tf.placeholder(tf.float32)
    learning_rate = tf.placeholder(tf.float32)
    
    input_layer = tf.reshape(x, [-1, 28, 28, 1])
    
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=num_filters,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)
    
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
    
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=num_filters,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)
    
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)
    
    pool2_flat = tf.reshape(pool2, [-1, 7 * 7 * num_filters])
    
    dense = tf.layers.dense(inputs=pool2_flat, units=128, activation=tf.nn.relu)
    
    logits = tf.layers.dense(inputs=dense, units=10)

    layers = [conv1, pool1, conv2, pool2, pool2_flat, dense, logits]
    
    # output: classification and probabilities
    classes = tf.argmax(input=logits, axis=1)
    softmax = tf.nn.softmax(logits, name="softmax_output")
    
    # one-hot encoding and cross entropy loss
    onehot_labels = tf.one_hot(indices=tf.cast(y, tf.int32), depth=10)
    loss = tf.losses.softmax_cross_entropy(
        onehot_labels=onehot_labels, logits=logits)
    
    # SGD
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    train = optimizer.minimize(loss)
    
    # accuracy = tf.metrics.accuracy(labels=y, predictions=classes)
    correct_prediction = tf.equal(classes, tf.cast(y, dtype=tf.int64))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    return train, accuracy, x, y, learning_rate, layers



# load data
Dtrain, Dval, Dtest = mnist()
x_train, y_train = Dtrain
x_valid, y_valid = Dval


init = tf.global_variables_initializer()
n_samples = len(x_train)
batch_size = 64
n_batches = n_samples // batch_size


def save_csv(filename, results):
    import csv
    keys = sorted(results.keys())
    with open(filename, "w") as outfile:
        writer = csv.writer(outfile, delimiter = "\t")
        writer.writerow(keys)
        writer.writerows(zip(*[results[key] for key in keys]))


def save_plot(filename, results, y_names):
    import matplotlib.pyplot as pp
    fig = pp.figure()
    x = results["Epoch"]
    for name in y_names:
        y = results[name]
        pp.plot(x, y, 'o-', label=name)
    pp.savefig(filename)


def examine_learning_rates(learning_rates=[0.1, 0.01, 0.001, 0.0001]):
    """Run with different learning rates, save accuracies and plot. Sheet 2, ex. 2."""
    names = ["LR=" + str(lr) for lr in learning_rates]
    results = dict()
    train, accuracy, x, y, learning_rate = network(num_filters=16)

    for l_rate in learning_rates:
        epochs = []
        results["LR=" + str(l_rate)] = []
        with tf.Session() as sess:
            sess.run(init)
            for e in range(10):
                for i in range(n_batches):
                    x_batch = x_train[i * batch_size : (i+1) * batch_size]
                    y_batch = y_train[i * batch_size : (i+1) * batch_size]
                    train.run(feed_dict={x: x_batch, y: y_batch, learning_rate: l_rate})
                
                    # sess.run(train, {x: x_train[:n_samples], y: y_train[:n_samples]})
                    epoch = e * n_batches + i
                    if epoch % 100 == 0:
                        acc = accuracy.eval({x: x_valid, y: y_valid})
                        results["LR=" + str(l_rate)].append(acc)
                        epochs.append(epoch)
                        # print(epoch, acc)

    results["Epoch"] = epochs
    save_csv("results.csv", results)
    save_plot("plots.png", results, names)


# methods for counting the number of trainable parameters:
# https://stackoverflow.com/questions/38160940/how-to-count-total-number-of-trainable-parameters-in-a-tensorflow-model

def count_number_trainable_params():
    '''
    Counts the number of trainable variables.
    '''
    tot_nb_params = 0
    for trainable_variable in tf.trainable_variables():
        shape = trainable_variable.get_shape() # e.g [D,F] or [W,H,C]
        print(trainable_variable, shape)
        current_nb_params = get_nb_params_shape(shape)
        tot_nb_params = tot_nb_params + current_nb_params
    return tot_nb_params


def get_nb_params_shape(shape):
    '''
    Computes the total number of params for a given shap.
    Works for any number of shapes etc [D,F] or [W,H,C] computes D*F and W*H*C.
    '''
    nb_params = 1
    for dim in shape:
        nb_params = nb_params*int(dim)
    return nb_params 


def examine_runtime(filters=[8, 32]):  #, 32, 64, 128])
    import time
    n_samples = 100
    results = dict(Params=[], Time=[])
    with tf.Session() as sess:
        sess.run(init)
        for num in filters:
            train, accuracy, x, y, learning_rate, layers = network(num)
            start = time.time()
            train.run(feed_dict={x: x_train[:n_samples], y: y_train[:n_samples],
                                 learning_rate: 0.01})
            end = time.time()
            results["Time"].append(end - start)
            # calculate number of parameters
            params = count_number_trainable_params()
            results["Params"].append(params)
    results["Filters"] = filters
    print(results)


examine_runtime()
            





