import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data

print("Reading MNIST data...")
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
print("Done reading MNIST data.")

training_data = mnist.train.images[:7000]

input_image = tf.placeholder(tf.float32)
input_image = input_image / 255.0  # rescale gray values
learning_rate = tf.placeholder(tf.float32)
samples = tf.placeholder(tf.float32)

input_layer = tf.reshape(input_image, [-1, 28, 28, 1])

activation = tf.nn.relu

conv1 = tf.layers.conv2d(
    inputs=input_layer,
    filters=8,
    kernel_size=[3, 3],
    padding="same",
    activation=activation)

pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

conv2 = tf.layers.conv2d(
    inputs=pool1,
    filters=4,
    kernel_size=[3, 3],
    padding="same",
    activation=activation)

pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

conv3 = tf.layers.conv2d(
    inputs=pool2,
    filters=2,
    kernel_size=[3, 3],
    padding="same",
    activation=activation)

conv_t1 = tf.layers.conv2d_transpose(
    inputs=conv3,
    filters=4,
    kernel_size=[2, 2],
    strides=2,
    padding="same",
    activation=activation)

conv_for_t1 = tf.layers.conv2d(
    inputs=conv_t1,
    filters=4,
    kernel_size=[3, 3],
    padding="same",
    activation=activation)

conv_t2 = tf.layers.conv2d_transpose(
    inputs=conv_for_t1,
    filters=8,
    kernel_size=[2, 2],
    strides=2,
    padding="same",
    activation=activation)

conv_for_t2 = tf.layers.conv2d(
    inputs=conv_t2,
    filters=8,
    kernel_size=[3, 3],
    padding="same",
    activation=activation)

output_layer = tf.layers.conv2d(
    inputs=conv_for_t2,
    filters=1,
    kernel_size=[1, 1],
    padding="same",
    activation=activation)

loss = tf.reduce_mean(tf.square(output_layer - input_layer))

# SGD
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train = optimizer.minimize(loss)

# extract weight norms
layers = [input_layer, conv1, pool1, conv2, pool2, conv3,
          conv_t1, conv_for_t1, conv_t2, conv_for_t2, output_layer]
weight_norms = [tf.norm(weights) for weights in layers]

init = tf.global_variables_initializer()
n_samples = len(training_data)
batch_size = 64
n_batches = n_samples // batch_size


def save_plot(filename, results, y_names):
    import matplotlib.pyplot as pp
    fig, ax = pp.subplots()
    x = results["Epoch"]
    for name in y_names:
        y = results[name]
        ax.plot(x, y, 'o-', label=name)
    ax.legend()
    pp.savefig(filename)


def save_csv(filename, results):
    import csv
    keys = sorted(results.keys())
    with open(filename, "w") as outfile:
        writer = csv.writer(outfile, delimiter = "\t")
        writer.writerow(keys)
        writer.writerows(zip(*[results[key] for key in keys]))


def examine_learning_rates(num_epochs=20, learning_rates=[0.1, 0.01, 0.001]):
    """Run with different learning rates, save losses and plot. Sheet 3."""
    names = ["LR=" + str(lr) for lr in learning_rates]
    results = dict()
    for l_rate in learning_rates:
        epochs = []
        results["LR=" + str(l_rate)] = []
        with tf.Session() as sess:
            sess.run(init)
            for e in range(num_epochs):
                for i in range(n_batches):
                    input_image_batch = training_data[i * batch_size : (i+1) * batch_size]
                    train.run(feed_dict={input_image: input_image_batch,
                                         learning_rate: l_rate})
                    # print("Batch {} of {} done.".format(i, n_batches))
                
                ls = loss.eval({input_image: training_data})
                results["LR=" + str(l_rate)].append(ls)
                epochs.append(e)
                print("Learning rate= {}, epoch {}, loss {}".format(l_rate, e, ls))
            weights = sess.run(weight_norms, feed_dict={input_image: None,
                                                        learning_rate: None})
            print("Norms of layer weights: {}".format(weights))

    results["Epoch"] = epochs
    save_csv("results.csv", results)
    save_plot("plots.png", results, names)


def save_sample_fig(sample_images, output_images,
                    distorted_input, distorted_output,
                    filename="sample_images.png"):
    import matplotlib.pyplot as pp
    n_examples = sample_images.shape[0]
    fig, axarr = pp.subplots(4, n_examples)
    for i in range(sample_images.shape[0]):
        axarr[0, i].imshow(sample_images[i, :].reshape(28, 28), cmap="gray")
        axarr[1, i].imshow(output_images[i, :].reshape(28, 28), cmap="gray")
        axarr[2, i].imshow(distorted_input[i, :].reshape(28, 28), cmap="gray")
        axarr[3, i].imshow(distorted_output[i, :].reshape(28, 28), cmap="gray")
    for ax in axarr.flat:
        ax.axis("off")
    pp.savefig(filename)
    

def show_examples(n_examples = 5, distortion=1e-2, epochs=15, l_rate=0.001, filename="sample_images.png"):
    """Show examples of the autoencoding results. Sheet 3, bonus exercise."""
    with tf.Session() as sess:
        sess.run(init)
        # first train the network:
        for e in range(epochs):
            for i in range(n_batches):
                input_image_batch = training_data[i * batch_size : (i+1) * batch_size]
                train.run(feed_dict={input_image: input_image_batch,
                                     learning_rate: l_rate})
        # show samples
        indices = np.random.randint(training_data.shape[0], size=n_examples)
        sample_images = training_data[indices, :]
        output_images = sess.run(output_layer, feed_dict={input_image: sample_images,
                                                          learning_rate: None})
        distorted_input = sample_images + np.random.normal(scale=0.01,
                                                           size=sample_images.shape)
        distorted_output = sess.run(output_layer, feed_dict={input_image: distorted_input,
                                                             learning_rate: None})
        save_sample_fig(sample_images, output_images,
                        distorted_input, distorted_output, filename)
        
    
    
# examine_learning_rates()
show_examples(n_examples=5,
              distortion=0.1,
              epochs=15)

