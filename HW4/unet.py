import tensorflow as tf
import numpy as np
from main import Data
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pp


input_images = tf.placeholder(tf.float32)
input_labels = tf.placeholder(tf.int64)
labels = tf.cast(tf.one_hot(input_labels, depth=2), tf.int64)

input_size = 300
output_size = 116
center = input_size // 2
begin = center - output_size // 2
cropped_input = input_images[:, begin:(begin + output_size),
                                begin:(begin + output_size), :]

#print(input_image.get_shape().as_list())

# Network architecture

input_layer = tf.reshape(input_images, [-1, 300, 300, 1])

layers = [input_layer]

# We build the layers dynamically so that the number of filters does
# not have to be hardcoded.

level_min = 5
level_max = 9

for level in range(level_min, level_max + 1):
    filters = 2 ** level
    # Initialize layers. Weights and biases will be initialized as
    # wished by default. Padding is 'valid' by default.
    conv1 = tf.layers.conv2d(
        inputs=layers[-1],
        filters=filters,
        kernel_size=[3, 3],
        activation=tf.nn.relu,
        name="conv1_down_{}".format(level))
    layers.append(conv1)
    conv2 = tf.layers.conv2d(
        inputs=layers[-1],
        filters=filters,
        kernel_size=[3, 3],
        activation=tf.nn.relu,
        name="conv2_down_{}".format(level))
    layers.append(conv2)
    pool = tf.layers.max_pooling2d(inputs=layers[-1],
                                   pool_size=[2, 2],
                                   strides=2,
                                   name="pool_{}".format(level))
    layers.append(pool)

layers = layers[:-1]

layer_dict = dict()
for layer in layers:
    layer_dict[layer.name] = layer

for level in range(level_max - 1, level_min - 1, -1):
    filters = 2 ** level
    conv_t = tf.layers.conv2d_transpose(
        inputs=layers[-1],
        filters=filters,
        kernel_size=[2, 2],
        strides=2,
        activation=tf.nn.relu,
        name="conv_t_{}".format(level))
    layers.append(conv_t)
    crop_origin = layer_dict["conv2_down_{}/Relu:0".format(level)]
    size = conv_t.get_shape()[1]
    center = crop_origin.get_shape()[1] // 2
    begin = center - size // 2
    crop = crop_origin[:, begin:(begin + size), begin:(begin + size), :]
    conv_input = tf.concat([conv_t, crop], -1)
    #conv_input.name = "conv_input_{}".format(level)  # tensors are immutable...
    layers.append(conv_input)
    conv1 = tf.layers.conv2d(
        inputs=layers[-1],
        filters=filters,
        kernel_size=[3, 3],
        activation=tf.nn.relu,
        name="conv1_up_{}".format(level))
    layers.append(conv1)
    conv2 = tf.layers.conv2d(
        inputs=layers[-1],
        filters=filters,
        kernel_size=[3, 3],
        activation=tf.nn.relu,
        name="conv2_up_{}".format(level))
    layers.append(conv2)
  
    
output_layer = tf.layers.conv2d(
        inputs=layers[-1],
        filters=2,
        kernel_size=[1, 1],
        activation=None,
        name="output")
layers.append(output_layer)

# check layer shapes
#for layer in layers:
#    print(layer.name, layer.get_shape())

# Loss
sizes = output_layer.get_shape().as_list()
size = sizes[1] * sizes[2]
losses = tf.nn.softmax_cross_entropy_with_logits(
    logits=output_layer,
    labels=labels,
    dim=-1)
loss = tf.reduce_sum(losses) / size

# Optimizer
optimizer = tf.train.AdamOptimizer(0.0001, 0.95, 0.99)
train = optimizer.minimize(loss)

# Accuracy
prediction = tf.argmax(output_layer, axis=-1)
correct_cell_pred = tf.cast(tf.equal(input_labels, prediction), tf.int64) * input_labels
num_correct = tf.reduce_sum(correct_cell_pred)
incorrect_cell_pred = tf.cast(tf.not_equal(input_labels, prediction), tf.int64) * prediction
num_incorrect = tf.reduce_sum(incorrect_cell_pred)
cells = tf.reduce_sum(input_labels)
accuracy = num_correct / (cells + num_incorrect)

# Training loop

data = Data()

init = tf.global_variables_initializer()

num_epochs = 40000
log_epochs = 100
n_samples = 7

def save_plot(filename, results, y_names):
    fig, ax = pp.subplots()
    x = results["Epoch"]
    for name in y_names:
        y = results[name]
        ax.plot(x, y, 'o-', label=name)
    ax.legend(loc="lower right")
    pp.savefig(filename)


def save_csv(filename, results):
    import csv
    keys = sorted(results.keys())
    with open(filename, "w") as outfile:
        writer = csv.writer(outfile, delimiter = "\t")
        writer.writerow(keys)
        writer.writerows(zip(*[results[key] for key in keys]))


def save_sample_fig(sample_images, output_images, labels,
                    filename="sample_images.png"):
    n_examples = len(sample_images)
    fig, axarr = pp.subplots(3, n_examples)
    for i in range(n_examples):
        axarr[0, i].imshow(sample_images[i].reshape(116, 116), cmap="gray")
        axarr[1, i].imshow(output_images[i], cmap="gray")
        axarr[2, i].imshow(labels[i], cmap="gray")
    for ax in axarr.flat:
        ax.axis("off")
    pp.savefig(filename)
         

epochs = []
training_accs = []
validation_accs = []

with tf.Session() as sess:
    sess.run(init)
    # train the network
    for e in range(num_epochs // log_epochs):
        training_accuracies = np.zeros(log_epochs)
        for i in range(log_epochs):
            training_images, training_labels = data.get_train_image_list_and_label_list()
            train.run(feed_dict={input_images: training_images,
                                 input_labels: training_labels})
            training_accuracies[i] = accuracy.eval({input_images: training_images,
                                                    input_labels: training_labels})

        training_accuracies = np.nan_to_num(training_accuracies)
        training_acc = np.mean(training_accuracies)
        validation_images, validation_labels = data.get_test_image_list_and_label_list()
        acc = accuracy.eval({input_images: validation_images,
                             input_labels: validation_labels})
        epochs.append(e * log_epochs)
        training_accs.append(training_acc)
        validation_accs.append(acc)
        print("Epoch {}: training accuracy {:.4f}, validation accuracy {:.4f}"
              .format(e * log_epochs, training_acc, acc))

    # save sample images
    validation_images, validation_labels = data.get_test_image_list_and_label_list()
    sample_images = cropped_input.eval({input_images: validation_images[:n_samples],
                                        input_labels: validation_labels[:n_samples]})
    output_images = prediction.eval({input_images: validation_images[:n_samples],
                                     input_labels: validation_labels[:n_samples]})

        
results = dict()
results["Epoch"] = epochs
results["Validation_accuracy"] = validation_accs
results["Training_accuracy"] = training_accs
save_csv("results.csv", results)
names = ["Training_accuracy", "Validation_accuracy"]
save_plot("plots.png", results, names)
save_sample_fig(sample_images, output_images, validation_labels[:n_samples])
