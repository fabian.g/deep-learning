from ho_exercise import objective_function_normalized, runtime_normalized
from robo.fmin import bayesian_optimization
import numpy as np

def trajectory(iterations=50, runs=10, verbose=False, runtime=False):
    # set lower and upper limits for (normalized) parameters
    lower = np.zeros(5)
    upper = np.ones(5)
    incumbents = np.zeros((iterations, runs))
    runtimes = np.zeros((iterations, runs))
    time = 0.0
    for r in range(runs):
        results = bayesian_optimization(objective_function_normalized, lower,
                                        upper, num_iterations=50)
        incumbent = results["incumbent_values"]
        incumbents[:, r] = incumbent        
        if verbose:
            print("Run: {}, validation error: {:.4f}, incumbents: {}"
                  .format(r, incumbent[-1], incumbent))
        if runtime:
            for (it, conf) in enumerate(results["X"]):
                runtimes[it, r] = runtime_normalized(conf)
    return (np.mean(incumbents, axis=1), np.cumsum(runtimes, axis=0))
