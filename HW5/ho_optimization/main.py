import bayesian
import random_search
import numpy as np

def save_plot(trajectories, names, description, path="trajectories.png"):
    import matplotlib.pyplot as pp
    fig = pp.figure()
    for (traj, name) in zip(trajectories, names):
        pp.plot(traj, label="{}: {}".format(name, description))
    pp.ylim(ymin=0.0)
    pp.legend()
    pp.savefig(path)

traj_random, _ = random_search.trajectory(runs=10, verbose=True)
traj_bayesian, _ = bayesian.trajectory(runs=10, verbose=True)
save_plot(trajectories=(traj_random, traj_bayesian),
          names=("Random search", "Bayesian optimization"),
          description="mean incumbent validation epoch by iteration",
          path="trajectories.png")


_, rt_random = random_search.trajectory(runs=1, runtime=True)
_, rt_bayesian = bayesian.trajectory(runs=1, runtime=True)
save_plot(trajectories=(rt_random, rt_bayesian),
          names=("Random search", "Bayesian optimization"),
          description="cumulated runtime by iteration",
          path="runtimes.png")
