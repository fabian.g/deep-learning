from ho_exercise import objective_function_normalized, runtime_normalized
import numpy as np

def trajectory(iterations=50, runs=10, verbose=False, runtime=False):
    # sample all parameters uniformly from the whole intervals and
    # deploy them to a function that expects them normalized (see
    # ho_exercise.py)
    incumbents = np.zeros((iterations, runs))
    runtimes = np.zeros((iterations, runs))
    time = 0.0
    for r in range(runs):
        incumbent = np.inf
        for it in range(iterations):
            x = np.random.uniform(size=5)
            y = objective_function_normalized(x)
            if runtime:
                runtimes[it, r] = runtime_normalized(x)
            incumbent = min(y, incumbent)
            if verbose:
                print("Run: {}, epoch: {}, validation error: {:.4f}, incumbent: {:.4f}"
                      .format(r, it, y, incumbent))
            incumbents[it, r] = incumbent
    return (np.mean(incumbents, axis=1), np.cumsum(runtimes, axis=0))
