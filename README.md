## Deep Learning Lab

Code and files to the deep learning lab course in winter term 2017/18.

Course repo: https://github.com/aisrobots/dl_lab_2017

Solutions to assignment 1 can be found in [HW1](/HW1/): [report_1.pdf](/HW1/report_1.pdf) and [exercice_1.ipynb](/HW1/exercise_1.ipynb).